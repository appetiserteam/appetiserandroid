package com.example.appetiser_demo.startup;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.appetiser_demo.R;
import com.example.appetiser_demo.base.BaseAppCompatActivity;
import com.example.appetiser_demo.detail.ItemDetailActivity;
import com.example.appetiser_demo.detail.ItemDetailFragment;
import com.example.appetiser_demo.list.ItemListActivity;
import com.example.appetiser_demo.local_storage.AppSharedPreference;
import com.example.appetiser_demo.model.ItemLastOpen;
import com.example.appetiser_demo.model.ItemModel;

/**
 * Default activity, initialize next page to open.
 * */
public class StartUpActivity extends BaseAppCompatActivity {
    public static final int RETURN_FROM_DETAIL  = 121;
    @Override
    public int getLayoutResource() {
        return R.layout.activity_startup;
    }

    @Override
    public void onCreate() {

        ItemLastOpen itemLastOpen = AppSharedPreference.GET.getPageLastOpen();

        if (itemLastOpen.hasSelectedItem && itemLastOpen.itemData != null){
            if (getResources().getBoolean(R.bool.isTablet)) {
                Intent intent = new Intent(this, ItemListActivity.class);
                intent.putExtra(ItemDetailFragment.ARG_ITEM, ItemModel.convert(itemLastOpen.itemData));
                startActivity(intent);
            }else {
                Intent intent = new Intent(this, ItemDetailActivity.class);
                intent.putExtra(ItemDetailFragment.ARG_ITEM, ItemModel.convert(itemLastOpen.itemData));
                startActivityForResult(intent,RETURN_FROM_DETAIL);
            }
        }else {
            Intent intent = new Intent(this, ItemListActivity.class);
            startActivity(intent);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RETURN_FROM_DETAIL){
            Intent intent = new Intent(this, ItemListActivity.class);
            startActivity(intent);
        }
    }
}
