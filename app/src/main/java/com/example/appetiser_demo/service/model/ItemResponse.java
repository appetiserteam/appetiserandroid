package com.example.appetiser_demo.service.model;

import com.example.appetiser_demo.model.ItemModel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Model class response for Item
 * */
public class ItemResponse implements Serializable {
    public int resultCount;
    public ArrayList<ItemModel> results = new ArrayList<>();

    public ItemResponse() {
    }
}
