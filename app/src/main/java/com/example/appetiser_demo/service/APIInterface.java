package com.example.appetiser_demo.service;

import com.example.appetiser_demo.service.model.ItemResponse;

import retrofit2.Call;
import retrofit2.http.GET;
/**
 * List of API endpoints
 * */
public interface APIInterface {

    @GET("search?term=star&amp;country=au&amp;media=movie&amp;all")
    Call<ItemResponse> getSearch();
}
