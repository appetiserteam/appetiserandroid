package com.example.appetiser_demo.service;

import androidx.annotation.NonNull;

import com.example.appetiser_demo.R;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Implements retrofit callback handling
 * Simplify response configuration
 * */
public class APICallback<T> implements Callback<T> {

    private Callback<T> callback;

    public APICallback(@NonNull Callback<T> callback) {
        this.callback = callback;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {

        if (response.isSuccessful()){
            if (response.body() != null){
                callback.onSuccess(response.body());
                return;
            }
        }

        try {
            callback.onFailure(response.errorBody().string());
        } catch (IOException e) {
            e.printStackTrace();
            callback.onFailure("Connection to the server failed.");
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        t.printStackTrace();
        callback.onFailure(t.getLocalizedMessage());

    }

    public interface Callback<T>{
        void onSuccess(T data);
        void onFailure(String message);

    }
}
