package com.example.appetiser_demo.service;

import android.content.res.Resources;

import com.example.appetiser_demo.R;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * API intialization and main class
 * */
public class APIService {
    private static final APIService ourInstance = new APIService();

    public static APIService getInstance() {
        return ourInstance;
    }


    private Retrofit retrofit;
    private APIInterface apiInterface;

    private APIService() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://itunes.apple.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterface = retrofit.create(APIInterface.class);
    }

    public APIInterface getApiInterface() {
        return apiInterface;
    }
}
