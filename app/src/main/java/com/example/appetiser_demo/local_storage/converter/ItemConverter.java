package com.example.appetiser_demo.local_storage.converter;

import com.example.appetiser_demo.local_storage.ItemEntity;
import com.example.appetiser_demo.model.ItemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates to convert {@link ItemModel} and {@link ItemEntity}
 * */
public class ItemConverter {
    public static ArrayList<ItemEntity> convert(ArrayList<ItemModel> list){
        ArrayList<ItemEntity> newList = new ArrayList<>();
        for (ItemModel item: list){
            ItemEntity entity = new ItemEntity();
            entity.trackId = item.trackId;
            entity.trackName = item.trackName;
            entity.trackViewUrl = item.trackViewUrl;
            entity.trackPrice = item.trackPrice;
            entity.artworkUrl30 = item.artworkUrl30;
            entity.artworkUrl60 = item.artworkUrl60;
            entity.artworkUrl100 = item.artworkUrl100;
            entity.country = item.country;
            entity.currency = item.currency;
            entity.longDescription = item.longDescription;
            entity.primaryGenreName = item.primaryGenreName;
            entity.releaseDate = item.releaseDate;
            newList.add(entity);
        }
        return newList;
    }

    public static ArrayList<ItemModel> revert(List<ItemEntity> list){
        ArrayList<ItemModel> newList = new ArrayList<>();
        for (ItemEntity entity: list){
            ItemModel item = new ItemModel();
            item.trackId = entity.trackId;
            item.trackName = entity.trackName;
            item.trackViewUrl = entity.trackViewUrl;
            item.trackPrice = entity.trackPrice;
            item.artworkUrl30 = entity.artworkUrl30;
            item.artworkUrl60 = entity.artworkUrl60;
            item.artworkUrl100 = entity.artworkUrl100;
            item.country = entity.country;
            item.currency = entity.currency;
            item.longDescription = entity.longDescription;
            item.primaryGenreName = entity.primaryGenreName;
            item.releaseDate = entity.releaseDate;
            newList.add(item);
        }
        return newList;
    }
}
