package com.example.appetiser_demo.local_storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.appetiser_demo.model.ItemLastOpen;
import com.example.appetiser_demo.model.ItemModel;

import java.text.SimpleDateFormat;
/**
 * Creates Shared preference implementation
 * */
public class AppSharedPreference {

    private static final String DATE_LAST_OPEN = "dateLastOpen";
    private static final String PAGE_LAST_OPEN = "pageLastOpen";

    public static AppSharedPreference GET = new AppSharedPreference();
    public SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public void initialize(Context context){
        preferences = context.getSharedPreferences("Appetiser-SharedPreference",
                Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    /**
     * Saves last date the application is used.
     * */
    public void setDateLastOpen(long date){
        editor.putLong(DATE_LAST_OPEN,date);
        editor.commit();
    }
    /**
     * Saves last page content open
     * @param itemLastOpen - model class for item last content {@link ItemLastOpen}
     * */
    public void setPageLastOpen(ItemLastOpen itemLastOpen){
        editor.putString(PAGE_LAST_OPEN, itemLastOpen.toString());
        editor.commit();
    }

    /**
     * Gets last date in string format
     * @return string - date in  date string format
     * */
    public String getDateLastOpen() {
        long date = preferences.getLong(DATE_LAST_OPEN,System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");

        return  format.format(date);
    }
    /**
     * Gets last page content
     * @return itemLastOpen - model class that hold item last open content {@link ItemLastOpen}
     * */
    public ItemLastOpen getPageLastOpen(){
        return ItemLastOpen.convert(preferences.getString(PAGE_LAST_OPEN,null));
    }
}
