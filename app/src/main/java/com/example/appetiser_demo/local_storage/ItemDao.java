package com.example.appetiser_demo.local_storage;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.appetiser_demo.model.ItemModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.http.GET;
/**
 * Dao for Item
 * */
@Dao
public interface ItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ArrayList<ItemEntity> list);

    @Query("SELECT * FROM ItemEntity")
    List<ItemEntity> getAll();
    @Delete
    void reset(ArrayList<ItemEntity> list);
}
