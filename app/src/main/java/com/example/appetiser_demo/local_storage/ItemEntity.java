package com.example.appetiser_demo.local_storage;


import androidx.room.Entity;
import androidx.room.PrimaryKey;
/**
 * Entity for Item
 * */
@Entity
public class ItemEntity {
    @PrimaryKey
    public long trackId;

    public String trackName;
    public String trackViewUrl;
    public double trackPrice;

    public String artworkUrl30;
    public String artworkUrl60;
    public String artworkUrl100;

    public String longDescription;
    public String currency;

    public String country;
    public String releaseDate;
    public String primaryGenreName;

    public ItemEntity() {
    }
}
