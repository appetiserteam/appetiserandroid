package com.example.appetiser_demo.local_storage;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.appetiser_demo.model.ItemModel;

/**
 * Creates room database for the app
 * */
@Database(entities = {ItemEntity.class},version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public static AppDatabase GET;

    public abstract ItemDao itemDao();
}
