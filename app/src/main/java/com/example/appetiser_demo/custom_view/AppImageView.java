package com.example.appetiser_demo.custom_view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.example.appetiser_demo.R;

/**
 * Custom ImageView
 * */
public class AppImageView extends AppCompatImageView {
    public AppImageView(Context context) {
        super(context);
    }

    public AppImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    /**
     * Set image drawable with url string using glide
     * @param urlString = the url link in string
     * @see Glide
     * */
    public void setImageUrlString(String urlString){
        Glide.with(getContext())
                .load(urlString)
                .centerCrop()
                .placeholder(R.drawable.ic_placeholder_filled)
                .into(this);
    }
}
