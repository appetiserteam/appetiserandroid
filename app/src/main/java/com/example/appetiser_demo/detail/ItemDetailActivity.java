package com.example.appetiser_demo.detail;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.example.appetiser_demo.R;
import com.example.appetiser_demo.base.BaseAppCompatActivity;
import com.example.appetiser_demo.custom_view.AppImageView;
import com.example.appetiser_demo.list.ItemListActivity;
import com.example.appetiser_demo.model.ItemModel;

import butterknife.BindView;

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ItemListActivity}.
 */
public class ItemDetailActivity extends BaseAppCompatActivity {

    /**
     * Initialize using butterknife {@link butterknife.ButterKnife}
     * */
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.imgArtWork)AppImageView imgArtWork;
    @BindView(R.id.textTrackName)TextView textTrackName;
    @BindView(R.id.textGenre)TextView textGenre;
    @BindView(R.id.textPrice)TextView textPrice;

    /**
     * Item data pass from intent
     * */
    private ItemModel itemModel;

    @Override
    public int getLayoutResource() {
        return R.layout.activity_item_detail;
    }

    @Override
    public void onCreate() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        itemModel = (ItemModel) getIntent().getSerializableExtra(ItemDetailFragment.ARG_ITEM);

        if (itemModel != null){
            textTrackName.setText(itemModel.trackName);
            textGenre.setText(itemModel.primaryGenreName);
            textPrice.setText(String.valueOf(itemModel.trackPrice));

            String urlString = itemModel.artworkUrl100 != null?itemModel.artworkUrl100:
                    itemModel.artworkUrl60 != null?itemModel.artworkUrl60:
                            itemModel.artworkUrl30 != null?itemModel.artworkUrl30: null;
            imgArtWork.setImageUrlString(urlString);

            /**
             * Add ItemDetailFragment in the activity {@link ItemDetailFragment}
             * */
            Bundle arguments = new Bundle();
            arguments.putSerializable(ItemDetailFragment.ARG_ITEM, itemModel);

            ItemDetailFragment fragment = new ItemDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();

        }

    }

    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
