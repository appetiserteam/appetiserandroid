package com.example.appetiser_demo.detail;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import com.example.appetiser_demo.R;
import com.example.appetiser_demo.base.BaseFragment;
import com.example.appetiser_demo.list.ItemListActivity;
import com.example.appetiser_demo.local_storage.AppSharedPreference;
import com.example.appetiser_demo.model.ItemLastOpen;
import com.example.appetiser_demo.model.ItemModel;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import butterknife.BindView;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends BaseFragment {
    /**
     * The fragment argument representing the ITEM that this fragment represents.
     */
    public static final String ARG_ITEM = "item";
    /**
     * Initialize using butterknife {@link butterknife.ButterKnife}
     * */
    @BindView(R.id.textDesc) TextView textDesc;
    @BindView(R.id.textNoDesc)TextView textNoDesc;

    /**
     * Holds item data passed from parent activity
     * */
    private ItemModel itemModel;

    public ItemDetailFragment() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_ITEM)) {
            itemModel = (ItemModel) getArguments().getSerializable(ARG_ITEM);

            /**
             * Set last page open content
             * @see AppSharedPreference
             * */
            AppSharedPreference.GET.setPageLastOpen(new ItemLastOpen(true,itemModel.toString()));
        }
    }
    @Override
    public int getLayoutResource() {
        return R.layout.item_detail;
    }

    @Override
    public void onCreateView() {
        /**
         * Set item description
         * */
        if (itemModel != null && itemModel.longDescription != null) {
            textDesc.setText(Html.fromHtml("<p>"+itemModel.longDescription+"</p>"));
            textNoDesc.setVisibility(View.GONE);
        }else {
            textNoDesc.setVisibility(View.VISIBLE);
        }
    }



}
