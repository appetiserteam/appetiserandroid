package com.example.appetiser_demo.list;

import androidx.annotation.NonNull;

import com.example.appetiser_demo.local_storage.AppDatabase;
import com.example.appetiser_demo.local_storage.ItemEntity;
import com.example.appetiser_demo.local_storage.converter.ItemConverter;
import com.example.appetiser_demo.model.ItemModel;
import com.example.appetiser_demo.service.model.ItemResponse;
import com.example.appetiser_demo.service.APICallback;
import com.example.appetiser_demo.service.APIService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
/**
 * Presenter class for {@link ItemListActivity}
 * Holds all logic function for ItemListActivity
 * */
public class ItemListPresenter {
    /**
     * Callback class
     * */
    private ItemListView view;

    /**
     * Disposable class for rx
     * */
    private CompositeDisposable disposable;

    public ItemListPresenter(@NonNull ItemListView view) {
        this.view = view;
        disposable = new CompositeDisposable();
    }

    /**
     * Method use to destroy disposable variable
     * */
    public void onDestroy(){
        if (!disposable.isDisposed())
            disposable.dispose();
    }

    /**
     * Get list of items from API calls
     * @param allowLoading = indicates to show or not the progressloaddialog
     * @see APIService
     * */
    public void getSearchItemList(boolean allowLoading){
        if (allowLoading) view.onStartProgress();
        APIService.getInstance().getApiInterface()
                .getSearch()
                .enqueue(new APICallback<ItemResponse>(
                        new APICallback.Callback<ItemResponse>() {
                    @Override
                    public void onSuccess(ItemResponse data) {
                        saveToLocal(data.results);
                        view.onGetItemListSuccess(data.results);
                        if (allowLoading) view.onStopProgress();
                    }

                    @Override
                    public void onFailure(String message) {
                        view.onGetItemListFailed(message);
                        if (allowLoading) view.onStopProgress();
                    }
                }));
    }
    /**
     * Save list of items to Local DB {@link AppDatabase}
     * @param list = list of items from API response
     * */
    public void saveToLocal(ArrayList<ItemModel> list){
    Completable completable =  Completable.fromAction(() -> {
        AppDatabase.GET.itemDao().insertAll(ItemConverter.convert(list));
    });
        disposable.add(
                completable.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(() -> {
                        }));
    }
    /**
     * Get list of items to Local DB {@link AppDatabase}
     * */
    public void getListFromLocal(){
        view.onStartProgress();
        Single<List<ItemEntity>> single = Single.fromCallable(() -> {
          return AppDatabase.GET.itemDao().getAll();
        });
        disposable.add(
                single.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe((list, throwable) -> {
                            if (list != null)
                                view.onGetItemListFromLocal(ItemConverter.revert(list));
                            else
                                view.onGetItemListFromLocal(new ArrayList<>());
                            view.onStopProgress();
                        }));
    }

}
