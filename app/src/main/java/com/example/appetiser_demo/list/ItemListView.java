package com.example.appetiser_demo.list;

import com.example.appetiser_demo.model.ItemModel;

import java.util.ArrayList;

/**
 * Call for item presenter {@link ItemListPresenter}
 * */
public interface ItemListView {

    /**
     * Called before request starts
     * */
    void onStartProgress();
    /**
     * Called after the request end
     * */
    void onStopProgress();

    /**
     * Called for local dababase result
     * @see com.example.appetiser_demo.local_storage.AppDatabase
     * */
    void onGetItemListFromLocal(ArrayList<ItemModel> itemList);

    /**
     * Called for API result
     * @see com.example.appetiser_demo.service.APIService
     * */
    void onGetItemListSuccess(ArrayList<ItemModel> itemList);

    /**
     * Called for request error/failure
     * */
    void onGetItemListFailed(String errorMessage);
}
