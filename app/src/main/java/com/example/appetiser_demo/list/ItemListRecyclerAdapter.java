package com.example.appetiser_demo.list;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appetiser_demo.R;
import com.example.appetiser_demo.custom_view.AppImageView;
import com.example.appetiser_demo.local_storage.AppDatabase;
import com.example.appetiser_demo.local_storage.AppSharedPreference;
import com.example.appetiser_demo.model.ItemModel;

import java.util.ArrayList;
/**
 * Extends recycler adapter for {@link ItemListActivity}
 * Sets item view
 * */
public class ItemListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /**
     * Callback for item list selection
     * @see Callback
     * */
    private Callback callback;

    /**
     * Arraylist of items
     * */
    private ArrayList<ItemModel> list = new ArrayList<>();

    /**
     * Flag position for seletec item
     * */
    private int itemSelectedPos = -1;

    public ItemListRecyclerAdapter(@NonNull Callback callback) {
        this.callback = callback;
    }

    public ItemListRecyclerAdapter(@NonNull Callback callback,ArrayList<ItemModel> list) {
        this.list = list;
        this.callback = callback;
    }

    /**
     * Sets item list
     * @param list - items
     * */
    public void setList(ArrayList<ItemModel> list) {
        this.list = list;
    }


    /**
     * Sets item selected at position
     * @param itemSelectedPos - int position of the item
     * */
    public void setItemSelectedPos(int itemSelectedPos) {
        this.itemSelectedPos = itemSelectedPos == -1? -1: itemSelectedPos+1;
    }

    /**
     * Get for itemSelectedPos
     * */
    public int getItemSelectedPos() {
        return itemSelectedPos;
    }

    /**
     * Set item selection
     * @param item - seletec item model
     * */
    public void setItemSelected(ItemModel item) {
        int cnt = 0;
        for (ItemModel itemModel: list){
            if (itemModel.trackId == item.trackId){
                setItemSelectedPos(cnt);
                notifyDataSetChanged();
                return;
            }
            cnt++;
        }
    }
    /**
     * Create View holder for header and child item list
     * @see HeaderViewHolder - for header view
     * @see ItemViewHolder - for item view
     * */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0){
            return new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_item_header,parent,false));
        }else {
            return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_item_cell,parent,false));
        }

    }
    /**
     * Populate data to thee view holder
     * @see HeaderViewHolder - for header view
     * @see ItemViewHolder - for item view
     * */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder.getItemViewType() == 0){
            HeaderViewHolder holder = (HeaderViewHolder)viewHolder;
            String dateStr = AppSharedPreference.GET.getDateLastOpen();
            holder.textHeader.setText(dateStr);
        }else {
            ItemModel item = list.get(position -1);
            ItemViewHolder holder = (ItemViewHolder)viewHolder;
            holder.textTrackName.setText(item.trackName);
            holder.textGenre.setText(item.primaryGenreName);
            holder.textPrice.setText(String.valueOf(item.trackPrice));

            String urlString = item.artworkUrl100 != null?item.artworkUrl100:
                    item.artworkUrl60 != null?item.artworkUrl60:
                            item.artworkUrl30 != null?item.artworkUrl30: null;
            holder.imgArtwork.setImageUrlString(urlString);

            if (itemSelectedPos == position){
                holder.itemView.setBackgroundColor(holder.itemView.getContext()
                        .getResources().getColor(R.color.colorPrimaryLight));
            }else {
                holder.itemView.setBackgroundColor(Color.WHITE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemSelectedPos = position;
                    callback.onItemSelected(item);
                    notifyDataSetChanged();
                }
            });
        }


    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return 0;
        return 1;
    }

    @Override
    public int getItemCount() {
        return list.size()+1;
    }

    /**
     * Extends ViewHolder for header view
     * */
    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView textHeader;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            textHeader = itemView.findViewById(R.id.textDate);
        }
    }
    /**
     * Extends ViewHolder for item view
     * */
    public class ItemViewHolder extends RecyclerView.ViewHolder {

        AppImageView imgArtwork;
        TextView textTrackName;
        TextView textGenre;
        TextView textPrice;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            imgArtwork = itemView.findViewById(R.id.imgArtwork);
            textTrackName = itemView.findViewById(R.id.textTrackName);
            textGenre = itemView.findViewById(R.id.textGenre);
            textPrice = itemView.findViewById(R.id.textPrice);
        }
    }

    /**
     * Callback for item selected
     * */
    public interface Callback{
        void onItemSelected(ItemModel itemModel);
    }


}
