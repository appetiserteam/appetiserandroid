package com.example.appetiser_demo.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appetiser_demo.base.BaseAppCompatActivity;
import com.example.appetiser_demo.detail.ItemDetailActivity;
import com.example.appetiser_demo.detail.ItemDetailFragment;
import com.example.appetiser_demo.R;
import com.example.appetiser_demo.local_storage.AppSharedPreference;
import com.example.appetiser_demo.model.ItemLastOpen;
import com.example.appetiser_demo.model.ItemModel;
import com.example.appetiser_demo.utility.AppUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.internal.EverythingIsNonNull;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends BaseAppCompatActivity implements ItemListView, ItemListRecyclerAdapter.Callback {
    /**
     * Initialize using butterknife {@link butterknife.ButterKnife}
     * */
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.textNoRecord) TextView textNoRecord;

    /**
     * Item activity presenter
     * @see ItemListPresenter
     * */
    private ItemListPresenter itemListPresenter;

    /**
     * Item recyclerview adapter
     * @see ItemListRecyclerAdapter
     * */
    private ItemListRecyclerAdapter itemAdapter;

    @Override
    public int getLayoutResource() {
        return R.layout.activity_item_list;
    }

    @Override
    public void onCreate() {
        itemListPresenter = new ItemListPresenter(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.app_name));

        /**
         * Set recyclerView layoutManager
         * */
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        /**
         * Add item decoration
         * */
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        /**
         * Set adapter
         * */
        itemAdapter = new ItemListRecyclerAdapter(this);
        recyclerView.setAdapter(itemAdapter);

        /**
         * Gets item list from local db
         * */
        itemListPresenter.getListFromLocal();

    }
    @Override
    protected void onResume() {
        super.onResume();
        /**
         * Set last page open content
         * */
        AppSharedPreference.GET.setPageLastOpen(new ItemLastOpen(false,null));

        /**
         * Update recyclerView adapter for item selected
         * */
        if (!getResources().getBoolean(R.bool.isTablet)){
            if (itemAdapter != null){
                itemAdapter.setItemSelectedPos(-1);
                itemAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onDestroy() {
        itemListPresenter.onDestroy();
        AppSharedPreference.GET.setDateLastOpen(System.currentTimeMillis());
        super.onDestroy();
    }

    /**
     * ItemListView callback methods
     * @see ItemListView
     * */
    @Override
    public void onStartProgress() {
        AppUtil.getInstance(this).showProgressDialog();
    }

    @Override
    public void onStopProgress() {
        AppUtil.getInstance(this).hideProgressDialog();
    }

    /**
     * Callback {@link ItemListView} for get list from local db result
     * @param itemList = item list
     * */
    @Override
    public void onGetItemListFromLocal(ArrayList<ItemModel> itemList) {
        setItemList(itemList);
        setLastItemSelected();

        if (AppUtil.getInstance(this).isNetworkConnected()){
            if (itemList == null) itemListPresenter.getSearchItemList(true);
            else if (itemList.size() == 0) itemListPresenter.getSearchItemList(true);
            else itemListPresenter.getSearchItemList(false);
        }else {
            Snackbar.make(recyclerView,getString(R.string.network_fail_message), Snackbar.LENGTH_LONG)
                    .setAction("Try again", v -> {
                        itemListPresenter.getSearchItemList(true);
                    });
        }
    }
    /**
     * Callback  {@link ItemListView} for get list from API
     * @param itemList = item list
     * */
    @Override
    public void onGetItemListSuccess(ArrayList<ItemModel> itemList) {
        setItemList(itemList);
        setLastItemSelected();
    }

    /**
     * Callback {@link ItemListView} to show message error
     * @param errorMessage = string message
     * */
    @Override
    public void onGetItemListFailed(String errorMessage) {
        AppUtil.getInstance(this).toast(errorMessage);
    }

    /**
     * Callback  {@link ItemListRecyclerAdapter} for selected item
     * @param itemModel = seleted item
     * */
    @Override
    public void onItemSelected(ItemModel itemModel) {
        /**
         * Show item details for tablet or for phone layout
         * */
        if (getResources().getBoolean(R.bool.isTablet)) {
            Bundle arguments = new Bundle();
            arguments.putSerializable(ItemDetailFragment.ARG_ITEM, itemModel);
            ItemDetailFragment fragment = new ItemDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, ItemDetailActivity.class);
            intent.putExtra(ItemDetailFragment.ARG_ITEM, itemModel);
            startActivity(intent);
        }
    }

    /**
    * Populate recyclerView items
    * @param itemList = current list of items
    * */
    private void setItemList(ArrayList<ItemModel> itemList){

        if (itemList == null ){
            textNoRecord.setVisibility(View.VISIBLE);
        }else if (itemList.size() == 0){
            textNoRecord.setVisibility(View.VISIBLE);
        }else {
            textNoRecord.setVisibility(View.GONE);
        }
        if (itemList != null){
            if (itemAdapter == null) {
                itemAdapter = new ItemListRecyclerAdapter(this,itemList);
                recyclerView.setAdapter(itemAdapter);
            }
            else {
                itemAdapter.setList(itemList);
                itemAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * Set last selected items
     * */
    private void setLastItemSelected(){
        if (itemAdapter != null && itemAdapter.getItemCount() > 0){
            ItemModel itemModel = (ItemModel) getIntent().getSerializableExtra(ItemDetailFragment.ARG_ITEM);
            if (itemModel != null){
                itemAdapter.setItemSelected(itemModel);
                onItemSelected(itemModel);
                recyclerView.getLayoutManager().scrollToPosition(itemAdapter.getItemSelectedPos());
            }


        }
    }
}