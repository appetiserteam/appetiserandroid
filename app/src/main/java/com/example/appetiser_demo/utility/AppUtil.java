package com.example.appetiser_demo.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.appetiser_demo.R;
/**
 * Custom utility class for the app
 * */
public class AppUtil {
    private static AppUtil ourInstance;

    public static AppUtil getInstance(Activity activity) {
        if (ourInstance == null) ourInstance = new AppUtil(activity);
        return ourInstance;
    }

    private Activity activity;
    private Dialog progressdialog;
    private AppUtil(Activity activity) {
        this.activity = activity;
    }
    /**
     * Initialize and show progressdialog
     * */
    public void showProgressDialog() {
        if (progressdialog == null) {
            progressdialog = new Dialog(activity);
            progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressdialog.setContentView(R.layout.custom_loading);
//            progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        progressdialog.setCancelable(false);
        progressdialog.show();
    }
    /**
     * hide progressdialog
     * */
    public void hideProgressDialog() {
        try {
            if (progressdialog.isShowing()) {
                progressdialog.dismiss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**
     * Toast implementation
     * @param message - the message
     * */
    public void toast(String message){
        Toast toast  = Toast.makeText(activity,message,Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
    /**
     * Checks network connection
     * */
    public boolean isNetworkConnected() {
        ConnectivityManager connectivity =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
