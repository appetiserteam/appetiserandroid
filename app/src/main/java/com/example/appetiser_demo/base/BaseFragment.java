package com.example.appetiser_demo.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
/**
 * Serves as base Fragment class for all created fragment classes
 * */
public abstract class BaseFragment extends Fragment {
    /**
     * Abstract for getting the raw layout resource
     * @return int of layout resource
     * */
    public abstract int getLayoutResource();

    /**
     * Calls to initialize view
     * */
    public abstract void onCreateView();

    private LayoutInflater inflater;
    private ViewGroup container;
    private Bundle saveInstance;


    /**
     * Set up content view.
     * Bind butternknife in the fragment
     * */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        this.saveInstance = savedInstanceState;
        View rootView = inflater.inflate(getLayoutResource(),container,false);
        ButterKnife.bind(this,rootView);
        onCreateView();

        return rootView;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public ViewGroup getContainer() {
        return container;
    }

    public Bundle getSaveInstance() {
        return saveInstance;
    }
}
