package com.example.appetiser_demo.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;

/**
 * Serves as base AppCompatActivity class for all created activity classes
 * */
public abstract class BaseAppCompatActivity extends AppCompatActivity {
    /**
     * Abstract for getting the raw layout resource
     * @return int of layout resource
     * */
    public abstract int getLayoutResource();

    /**
     * Method that functions same as onCreate(@Nullable Bundle savedInstanceState)
     * */
    public abstract void onCreate();

    /**
     * Carry the value of saveInstance of the activity class
     * */
    private Bundle saveInstance;


    /**
     * Set up content view.
     * Bind butternknife in the activity
     * */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);
        saveInstance = savedInstanceState;
        onCreate();
    }

    public Bundle getSaveInstance() {
        return saveInstance;
    }
}
