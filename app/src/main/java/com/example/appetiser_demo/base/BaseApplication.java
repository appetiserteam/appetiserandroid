package com.example.appetiser_demo.base;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.room.Room;

import com.example.appetiser_demo.local_storage.AppDatabase;
import com.example.appetiser_demo.local_storage.AppSharedPreference;

/**
 * extends Application class
 * */
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        /**
         * Initialize room database
         * @see AppDatabase
         * */
        AppDatabase.GET = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "Appetiser-Database").build();

        /**
         * Initialize app shared preference
         * @see AppSharedPreference
         * */
        AppSharedPreference.GET.initialize(this);
    }

}
