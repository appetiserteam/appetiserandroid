package com.example.appetiser_demo.model;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.io.Serializable;
/**
 * Model class for item last page content
 * */
public class ItemLastOpen implements Serializable {
    public boolean hasSelectedItem;
    public String itemData;

    public ItemLastOpen() {
    }

    public ItemLastOpen(boolean hasSelectedItem, String itemData) {
        this.hasSelectedItem = hasSelectedItem;
        this.itemData = itemData;
    }

    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this,getClass());

    }

    public static ItemLastOpen convert(String data){
        if (data == null)
            return new ItemLastOpen(false,null);

        return new Gson().fromJson(data,ItemLastOpen.class);
    }
}
