package com.example.appetiser_demo.model;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.io.Serializable;
/**
 * Model class for item
 * */
public class ItemModel implements Serializable {

    public String kind;

    public long artistId;
    public String artistName;
    public String artistViewUrl;

    public long trackId;
    public String trackName;
    public String trackViewUrl;
    public double trackPrice;


    public long collectionId;
    public String collectionName;
    public String collectionViewUrl;
    public double collectionPrice;

    public String artworkUrl30;
    public String artworkUrl60;
    public String artworkUrl100;

    public String longDescription;
    public String currency;

    public String country;
    public String releaseDate;
    public String primaryGenreName;

    public ItemModel() {
    }

    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this,getClass());
    }
    public static ItemModel convert(String data){
        return new Gson().fromJson(data,ItemModel.class);
    }
}
