# AppetiserAndroid

# Uses Model View Presenter architecture
  - I use this because it separate the business logic functions to the views (UI implementation).
  - By this, UI can easily change without worrying that might break important functions, and saves time on the changes as well.
  - Another is your code is easier to understand and developers can easily follow the structure because its well refactored.

# Dependencies
 1. Retrofit - for API calls
 2. Butterknife - binds fields, methods, view for easier intialization and access
 3. Glide - image load handling
 4. Room persistence - local database
 5. Rxjava for android - helps on background processes
 6. others : Gson, Android support libraries
  

